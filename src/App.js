import React, { Component } from "react";
import { BrowserRouter as Router, Route } from "react-router-dom";

import CustomNavbar from "./components/CustomNavbar";
import AddingToDatabase from "./components/routes/AddingToDatabase";
import DeletingFromDatabase from "./components/routes/DeletingFromDatabase";
import UpdatingDatabase from "./components/routes/UpdatingDatabase";
import Dashboard from "./components/routes/Dashboard";

import './custom.css';

class App extends Component {
  state = {
    colors: ["secondary", "secondary", "secondary"],
  }
  onClick = (e) => {
    let colors = ["secondary", "secondary", "secondary"];
    colors[e.target.value] = "primary";
    this.setState({colors});
  }

  resetColors = () => {
    this.setState({colors: ["secondary", "secondary", "secondary"]});
  }

  render() {
    return (
      <Router>
        <div>
          <CustomNavbar />
          <Route path="/add" render={(props)=><AddingToDatabase {...props} colors={this.state.colors} onClick={this.onClick} resetColors={this.resetColors}/>}/>
          <Route path="/delete" component={DeletingFromDatabase}/>
          <Route path="/update" render={(props)=><UpdatingDatabase {...props} colors={this.state.colors} onClick={this.onClick} resetColors={this.resetColors}/>}/>
          <Route path="/" exact component={Dashboard}/>
        </div>
      </Router>
    );
  }
}

export default App;
