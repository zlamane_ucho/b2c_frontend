import React from "react";
import { Navbar, Nav, NavDropdown } from "react-bootstrap";
import { Link } from "react-router-dom";

const CustomNavbar = () => {
  return (
    <Navbar bg="light" expand="lg">
      <Navbar.Brand as={Link} to="/">Bee2Code</Navbar.Brand>
      <Navbar.Toggle aria-controls="basic-navbar-nav" />
      <Navbar.Collapse id="basic-navbar-nav">
        <Nav className="mr-auto">
        <Nav.Link as={Link} to="/">Dashboard</Nav.Link>
          <NavDropdown title="Database actions" id="basic-nav-dropdown">
            <NavDropdown.Item as={Link} to="/add">Add</NavDropdown.Item>
            <NavDropdown.Item as={Link} to="/delete">Delete</NavDropdown.Item>
            <NavDropdown.Item as={Link} to="/update">Update</NavDropdown.Item>
          </NavDropdown>
        </Nav>
      </Navbar.Collapse>
    </Navbar>
  );
};

export default CustomNavbar;
