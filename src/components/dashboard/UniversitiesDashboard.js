import React, { Component } from "react";
import Select from "react-select";
import { Container, Col, Row, Jumbotron, Badge } from "react-bootstrap";

class UniversitiesDashboard extends Component {
  constructor(props) {
    super(props);

    this.state = {
      universitiesOption: null,
      universities: [],
      universitiesData: {},
      numberOfStudents: 0,
      numberOfFields: 0
    };
  }
  componentDidMount() {
    this.getUniversities();
  }
  getUniversities = () => {
    fetch("/university/all")
      .then(res => {
        if (!res.ok) throw Error(res.statusText);
        return res.json();
      })
      .then(universities => {
        const mapedUniversities = universities.map(university => {
          return {
            value: university.id,
            label: university.name
          };
        });
        const universitiesData = {};
        universities.forEach(university => {
          universitiesData[university.id] = university;
        });

        if (mapedUniversities.length) {
          this.setState({
            universities: mapedUniversities,
            universitiesOption: mapedUniversities[0],
            universitiesData
          });
          this.getStudentsCount();
        } else {
          this.setState({
            universitiesOption: null,
            universities: [],
            universitiesData: {},
            numberOfStudents: 0,
            numberOfFields: 0
          });
        }
      })
      .catch(err => {
        console.log(err);
      });
  };

  getStudentsCount = () => {
    const universityId = this.state.universitiesOption.value;

    fetch(`/university/countStudents/${universityId}`)
      .then(res => res.json())
      .then(numberOfStudents => {
        this.setState({ numberOfStudents });
        this.getFieldsCount();
      })
      .catch(err => console.log(err));
  };

  getFieldsCount = () => {
    const universityId = this.state.universitiesOption.value;
    fetch(`/university/countFields/${universityId}`)
      .then(res => res.json())
      .then(numberOfFields => {
        this.setState({ numberOfFields });
      })
      .catch(err => console.log(err));
  };

  universityHandler = universitiesOption => {
    this.setState({ universitiesOption });
    this.getStudentsCount();
  };
  render() {
    const {
      universitiesOption,
      universities,
      universitiesData,
      numberOfStudents,
      numberOfFields
    } = this.state;
    let uniDataJumbotron = null;
    if (universitiesOption) {
      let selectedId = universitiesOption.value;
      const { name, address, email, phone } = universitiesData[selectedId];
      uniDataJumbotron = (
        <Jumbotron>
          <Container>
            <h3 as={Row}>
              <Col lg="10">{name}</Col>
            </h3>
            <p>
              <Badge variant="secondary">Address</Badge> {address}
            </p>
            <p>
              <Badge variant="secondary">Email</Badge> {email}
            </p>
            <p>
              <Badge variant="secondary">Phone number</Badge> {phone}
            </p>
            <p>
              <Badge variant="secondary">No. of students</Badge>{" "}
              {numberOfStudents}
            </p>
            <p>
              <Badge variant="secondary">No. of fields</Badge>{" "}
              {numberOfFields}
            </p>
          </Container>
        </Jumbotron>
      );
    }

    return (
      <div>
        <Select
          value={universitiesOption}
          onChange={this.universityHandler}
          options={universities}
        />
        {uniDataJumbotron}
      </div>
    );
  }
}

export default UniversitiesDashboard;
