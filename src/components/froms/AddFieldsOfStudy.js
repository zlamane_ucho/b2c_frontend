import React, { Component } from "react";
import {
  Button,
  Container,
  Form,
  ListGroup,
  Alert,
  Col,
  Row
} from "react-bootstrap";

import FieldOfStudyBar from "./formComponents/FieldOfStudyBar";

class AddFieldsOfStudy extends Component {
  constructor(props) {
    super(props);

    this.state = {
      universities: [],
      addedFields: [],
      fieldFormData: {
        name: "",
        startDate: "",
        studentsLimit: ""
      },
      selectedUniversityID: "",
      errors: [],
      fetchError: false,
      success: "",
      loading: true
    };
  }

  componentDidMount() {
    fetch("/university/all")
      .then(res => {
        if (!res.ok) throw Error(res.statusText);
        return res.json();
      })
      .then(universities => {
        this.setState({
          universities,
          selectedUniversityID: universities[0].id,
          fetchError: false,
          loading: false
        });
      })
      .catch(err => {
        this.setState({ loading: false, fetchError: true });
      });
  }

  prepareOptions = () => {
    const { universities } = this.state;
    return universities.map(uni => {
      return (
        <option key={uni.id} value={uni.id}>
          {uni.name}
        </option>
      );
    });
  };

  validateFieldData = (name, startDate, studentsLimit) => {
    let errors = [];
    if (!name) errors.push("Add field name");
    if (!startDate) errors.push("Add field start date");
    if (!studentsLimit) errors.push("Add field students limit");

    return errors;
  };

  addField = e => {
    e.preventDefault();
    const { name, startDate, studentsLimit } = this.state.fieldFormData;

    const errors = this.validateFieldData(name, startDate, studentsLimit);
    if (errors.length) {
      this.setState({ errors });
      return;
    }

    let addedFields = [...this.state.addedFields];
    const key = Math.random()
      .toString(36)
      .substr(2, 9);
    addedFields.push({
      key,
      name,
      startDate,
      studentsLimit
    });

    const cleanFormFields = {
      name: "",
      startDate: "",
      studentsLimit: ""
    };

    this.setState({ addedFields, fieldFormData: cleanFormFields, errors: [] });
  };

  changeHandler = e => {
    let fieldFormData = { ...this.state.fieldFormData };

    //Students limit -> ONLY WHOLE POSITIVE VALUES
    if (e.target.id === "studentsLimit" && !/^[0-9]*$/.test(e.target.value))
      return;

    fieldFormData[e.target.id] = e.target.value;
    this.setState({ fieldFormData });
  };

  deleteHandler = e => {
    const { addedFields } = this.state;
    const key = e.target.value;

    const updatedFields = addedFields.filter(field => field.key !== key);

    this.setState({ addedFields: updatedFields });
  };

  selectHandler = e => {
    this.setState({ selectedUniversityID: e.target.value });
  };

  submitHandler = e => {
    e.preventDefault();
    const { addedFields, selectedUniversityID } = this.state;
    if (!addedFields.length) {
      this.setState({ errors: ["Add some fields of study"] });
      return;
    }

    const body = addedFields.map(field => {
      const { name, studentsLimit, startDate } = field;
      return {
        nameOfStudyField: name,
        universityId: selectedUniversityID,
        studentsLimit,
        startDate
      };
    });

    fetch("/fieldofstudy/add", {
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json"
      },
      method: "POST",
      body: JSON.stringify(body)
    })
      .then(res => {
        if (!res.ok) throw Error(res.statusText);

        return res.json();
      })
      .then(field => {
        if (field) {
          this.setState({
            success: `Successfully added ${
              field.length > 2 ? "fields" : "field"
            } to database`,
            addedFields: []
          });
          setTimeout(() => {
            this.setState({ success: "" });
          }, 4000);
        }
      })
      .catch(err => {
        this.setState({
          errors: ["Something went wrong. Check console for more information"]
        });
        console.log(err);
      });
  };

  render() {
    const {
      universities,
      addedFields,
      errors,
      success,
      fetchError,
      loading
    } = this.state;
    const { name, startDate, studentsLimit } = this.state.fieldFormData;

    if (fetchError)
      return (
        <Alert variant="danger">
          Coudn't get data from database. Please try later.
        </Alert>
      );
    if (loading)
      return <Alert variant="primary">Loading data... please wait.</Alert>;
    if (!universities.length) {
      return (
        <Alert variant="primary">
          You need to add some univerisities first
        </Alert>
      );
    } else {
      return (
        <Container>
          <Form as={Row} onSubmit={this.submitHandler}>
            <Col lg={{ span: 10, offset: 2 }}>
              <Form.Control as="select" onChange={this.selectHandler}>
                {this.prepareOptions()}
              </Form.Control>

              <ListGroup>
                {addedFields.map(({ key, name, startDate, studentsLimit }) => {
                  return (
                    <FieldOfStudyBar
                      key={key}
                      id={key}
                      name={name}
                      startDate={startDate}
                      studentsLimit={studentsLimit}
                      delete={this.deleteHandler}
                    />
                  );
                })}
              </ListGroup>
              <Button variant="success" type="submit">
                Save all fields
              </Button>
            </Col>
          </Form>
          <Form onSubmit={this.addField}>
            <Form.Group as={Row}>
              <Form.Label column lg="2">
                Field name
              </Form.Label>
              <Col lg="10">
                <Form.Control
                  id="name"
                  name="name"
                  type="text"
                  value={name}
                  onChange={this.changeHandler}
                  placeholder="Computer Programming"
                />
              </Col>
            </Form.Group>
            <Form.Group as={Row}>
              <Form.Label column lg="2">
                Start Date
              </Form.Label>
              <Col lg="10">
                <Form.Control
                  id="startDate"
                  name="startDate"
                  type="date"
                  value={startDate}
                  onChange={this.changeHandler}
                  placeholder="01-10-2019"
                />
              </Col>
            </Form.Group>
            <Form.Group as={Row}>
              <Form.Label column lg="2">
                Students' limit
              </Form.Label>
              <Col lg="10">
                <Form.Control
                  id="studentsLimit"
                  name="studentsLimit"
                  type="text"
                  value={studentsLimit}
                  onChange={this.changeHandler}
                  placeholder="30"
                />
              </Col>
            </Form.Group>

            {errors.length > 0 && (
              <Alert variant="warning">
                {errors.map(err => {
                  return (
                    <li
                      key={Math.random()
                        .toString(36)
                        .substr(2, 9)}
                    >
                      {err}
                    </li>
                  );
                })}
              </Alert>
            )}
            {success.length > 0 && <Alert variant="success">{success}</Alert>}
            <Row>
              <Col lg={{ span: 4, offset: 2 }}>
                <Button variant="success" type="submit">
                  Add Field
                </Button>
              </Col>
            </Row>
          </Form>
        </Container>
      );
    }
  }
}

export default AddFieldsOfStudy;
