import React, { Component } from "react";
import {
  Container,
  Form,
  Button,
  InputGroup,
  FormControl,
  Alert,
  Col,
  Row
} from "react-bootstrap";

class AddStudent extends Component {
  constructor(props) {
    super(props);

    this.state = {
      universities: [],
      fieldsOfStudy: [],
      selectedUniversityID: "",
      choosedFields: [],
      studentFormData: {
        firstName: "",
        lastName: "",
        email: "",
        phone: "",
        age: "",
        gender: "MALE",
        studentStatus: "ACTIVE"
      },
      grades: {},
      errors: [],
      success: "",
      loading: true,
      fetchError: false
    };
  }

  componentDidMount() {
    fetch("/university/all")
      .then(res => {
        if (!res.ok) throw Error(res.statusText);
        return res.json();
      })
      .then(universities => {
        this.setState({
          universities,
          selectedUniversityID: universities[0].id
        });
        fetch(`/fieldofstudy/universityId/${universities[0].id}`)
          .then(res => {
            if (!res.ok) throw Error(res.statusText);
            return res.json();
          })
          .then(fieldsOfStudy =>
            this.setState({ fieldsOfStudy, loading: false, fetchError: false })
          );
      })
      .catch(err => {
        this.setState({ loading: false, fetchError: true });
      });
  }

  fieldsHandler = e => {
    let choosedFields = [...this.state.choosedFields];
    if (e.target.checked) {
      choosedFields.push(e.target.value);
    } else {
      choosedFields.splice(choosedFields.indexOf(e.target.value), 1);
    }
    this.setState({ choosedFields });
  };

  inputHandler = e => {
    let studentFormData = { ...this.state.studentFormData };

    //AGE -> ONLY WHOLE POSITIVE VALUES
    if (e.target.id === "age" && !/^[0-9]*$/.test(e.target.value)) return;

    studentFormData[e.target.id] = e.target.value;
    this.setState({ studentFormData });
  };

  prepareOptions = () => {
    const { universities } = this.state;
    return universities.map(uni => {
      return (
        <option key={uni.id} value={uni.id}>
          {uni.name}
        </option>
      );
    });
  };

  prepareInputGroup = () => {
    const { fieldsOfStudy, choosedFields } = this.state;

    return fieldsOfStudy.map(field => {
      return (
        <Col key={field.id} lg="6">
          <InputGroup className="mb-3">
            <InputGroup.Prepend>
              <InputGroup.Checkbox
                onChange={this.fieldsHandler}
                value={field.id}
                checked={choosedFields.includes(field.id)}
              />
              <InputGroup.Text>{field.nameOfStudyField}</InputGroup.Text>
            </InputGroup.Prepend>
            <FormControl
              onChange={this.gradesHandler}
              id={field.id}
              value={
                this.state.grades[field.id] ? this.state.grades[field.id] : ""
              }
              placeholder="Grade e.g. 4.5"
            />
          </InputGroup>
        </Col>
      );
    });
  };

  selectHandler = e => {
    this.setState({ selectedUniversityID: e.target.value });
    fetch(`/fieldofstudy/universityId/${e.target.value}`)
      .then(res => {
        if (!res.ok) throw Error(res.statusText);
        return res.json();
      })
      .then(fieldsOfStudy =>
        this.setState({ fieldsOfStudy, fetchError: false })
      )
      .catch(err => {
        this.setState({ fetchError: true });
        console.log(err);
      });
  };

  validateStudentData = ({ firstName, lastName, email, phone, age }) => {
    let errors = [];
    if (!firstName) errors.push("Add student's first name");
    if (!lastName) errors.push("Add student's lat name");
    if (!email) errors.push("Add student's email");
    if (!/^\w+@\w+\.\w{2,3}$/.test(email)) errors.push("Incorrect email");
    if (!age) errors.push("Add student's age");
    if (age < 18 || age > 100) errors.push("Age from 18 to 100");
    if (!/^\d{3}-?\d{3}-?\d{3}$/.test(phone))
      errors.push("Wrong phone number. Correct form: 123456789 or 123-456-789");

    return errors;
  };

  submitHandler = e => {
    e.preventDefault();

    const {
      studentFormData,
      selectedUniversityID,
      choosedFields,
      grades
    } = this.state;

    //Validation
    const errors = this.validateStudentData(studentFormData);

    if (errors.length) {
      this.setState({ errors });
      return;
    } else {
      this.setState({ errors: [] });
    }

    const student = {
      ...studentFormData
    };

    if (!choosedFields.length) student.averageGrade = 0;
    else {
      let avgGrade = 0;
      let noGrade = false;
      choosedFields.forEach(field => {
        if (!grades[field]) {
          this.setState({ errors: ["Add grade to every checked field!"] });
          noGrade = true;
          return;
        }
        avgGrade += Number.parseFloat(grades[field]);
      });
      student.averageGrade =
        Math.round((avgGrade * 100) / choosedFields.length) / 100;

      if (noGrade) return;
    }

    fetch(`/student/add/${selectedUniversityID}`, {
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json"
      },
      method: "POST",
      body: JSON.stringify(student)
    })
      .then(res => {
        if (!res.ok) throw Error(res.statusText);
        return res.json();
      })
      .then(student => {
        const studentGrades = choosedFields.map(field => {
          return {
            fieldId: field,
            grade: grades[field],
            studentId: student.id
          };
        });
        fetch("/studentsGrades/add", {
          headers: {
            Accept: "application/json",
            "Content-Type": "application/json"
          },
          method: "POST",
          body: JSON.stringify(studentGrades)
        })
          .then(res => {
            if (!res.ok) throw Error(res.statusText);
            return res.json();
          })
          .then(studentGrades => {
            if (studentGrades) {
              this.setState({
                success: "Successfully added student to database",
                choosedFields: [],
                studentFormData: {
                  firstName: "",
                  lastName: "",
                  email: "",
                  phone: "",
                  age: "",
                  gender: "MALE",
                  studentStatus: "ACTIVE"
                },
                grades: {},
                fetchError: false,
                loading: false
              });
              setTimeout(() => {
                this.setState({ success: "" });
              }, 4000);
            }
          });
      })
      .catch(err => {
        this.setState({ fetchError: true });
        console.log(err);
      });
  };

  gradesHandler = e => {
    const value = e.target.value;
    //TODO Grade error
    //Grade range 2.0 - 5.0 changing by 0.5
    if (!/^(2|3|4|5)\.?(0|5)?$/.test(value) && !/$^/.test(value)) return;
    if (Number.parseFloat(value, 10) > 5.0 || value.length > 3) return;
    let grades = { ...this.state.grades };
    grades[e.target.id] = value;
    this.setState({ grades });
  };

  render() {
    const {
      universities,
      fieldsOfStudy,
      errors,
      success,
      loading,
      fetchError
    } = this.state;
    const {
      firstName,
      lastName,
      email,
      phone,
      age,
      gender,
      studentStatus
    } = this.state.studentFormData;
    if (fetchError)
      return (
        <Alert variant="danger">
          Coudn't get data from database. Please try later.
        </Alert>
      );
    if (loading)
      return <Alert variant="primary">Loading data... please wait.</Alert>;
    if (!universities.length)
      return (
        <Alert variant="primary">
          You need to add some univerisities first
        </Alert>
      );
    return (
      <Container>
        <Form onSubmit={this.submitHandler}>
          <Form.Control as="select" onChange={this.selectHandler}>
            {universities.length > 0 && this.prepareOptions()}
          </Form.Control>
          <Form.Group as={Row}>
            {fieldsOfStudy.length > 0 && this.prepareInputGroup()}
          </Form.Group>
          <Form.Group as={Row}>
            <Form.Label column lg="2">
              First name
            </Form.Label>
            <Col lg="4">
              <Form.Control
                id="firstName"
                name="firstName"
                type="text"
                onChange={this.inputHandler}
                value={firstName}
                placeholder="First name"
              />
            </Col>
            <Form.Label column lg="2">
              Last name
            </Form.Label>
            <Col lg="4">
              <Form.Control
                id="lastName"
                name="lastName"
                type="text"
                onChange={this.inputHandler}
                value={lastName}
                placeholder="Last name"
              />
            </Col>
          </Form.Group>
          <Form.Group as={Row}>
            <Form.Label column lg="2">
              Email
            </Form.Label>
            <Col lg="4">
              <Form.Control
                id="email"
                name="email"
                type="email"
                onChange={this.inputHandler}
                value={email}
                placeholder="example@mail.com"
              />
            </Col>
            <Form.Label column lg="2">
              Phone
            </Form.Label>
            <Col lg="4">
              <Form.Control
                id="phone"
                name="phone"
                type="text"
                onChange={this.inputHandler}
                value={phone}
                placeholder="765-831-932"
              />
            </Col>
          </Form.Group>
          <Form.Group as={Row}>
            <Form.Label column lg="2">
              Age
            </Form.Label>
            <Col lg="4">
              <Form.Control
                id="age"
                name="age"
                type="text"
                onChange={this.inputHandler}
                value={age}
                placeholder="Age"
              />
            </Col>
            <Form.Label column lg="2">
              Gender
            </Form.Label>
            <Col lg="4">
              <Form.Control
                id="gender"
                name="gender"
                as="select"
                onChange={this.inputHandler}
                value={gender}
              >
                <option value="MALE">Male</option>
                <option value="FEMALE">Female</option>
              </Form.Control>
            </Col>
          </Form.Group>
          <Form.Group as={Row}>
            <Form.Label column lg="2">
              Status
            </Form.Label>
            <Col lg="4">
              <Form.Control
                id="studentStatus"
                name="studentStatus"
                as="select"
                onChange={this.inputHandler}
                value={studentStatus}
              >
                <option value="ACTIVE">Active</option>
                <option value="INACTIVE">Inactive</option>
                <option value="SUSPENDED">Suspended</option>
                <option value="DEANS_LEAVE">Dean's Leave</option>
              </Form.Control>
            </Col>
            <Col lg={{ span: 4, offset: 2 }}>
              <Form.Label>
                <Button variant="success" type="submit">
                  Add student
                </Button>
              </Form.Label>
            </Col>
          </Form.Group>

          {errors.length > 0 && (
            <Alert variant="warning">
              {errors.map(err => {
                return (
                  <li
                    key={Math.random()
                      .toString(36)
                      .substr(2, 9)}
                  >
                    {err}
                  </li>
                );
              })}
            </Alert>
          )}
          {success.length > 0 && <Alert variant="success">{success}</Alert>}
        </Form>
      </Container>
    );
  }
}

export default AddStudent;
