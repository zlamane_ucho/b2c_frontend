import React, { Component } from "react";
import { Container, Form, Button, Alert, Row, Col } from "react-bootstrap";

class AddUniversity extends Component {
  constructor(props) {
    super(props);

    this.state = {
      universityFormData: {
        name: "",
        address: "",
        email: "",
        phone: ""
      },
      errors: [],
      success: ""
    };
  }

  validateUniversityForm = ({ name, address, email, phone }) => {
    let errors = [];
    if (!name) errors.push("Add university name");
    if (!address) errors.push("Add university adress");
    if (!email) errors.push("Add university email");
    if (!/^\w+@\w+\.\w{2,3}$/.test(email)) errors.push("Incorrect email");
    if (!/^\d{3}-?\d{3}-?\d{3}$/.test(phone))
      errors.push("Wrong phone number. Correct form: 123456789 or 123-456-789");

    return errors;
  };

  submitHandler = e => {
    e.preventDefault();

    let data = { ...this.state.universityFormData };

    const errors = this.validateUniversityForm(data);

    if (errors.length) {
      this.setState({ errors });
      return;
    } else {
      this.setState({ errors: [] });
    }

    data.additionDate = new Date().getTime();

    fetch("/university/add", {
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json"
      },
      method: "POST",
      body: JSON.stringify(data)
    })
      .then(res => {
        if (!res.ok) throw Error(res.statusText);

        return res.json();
      })
      .then(university => {
        if (university) {
          this.setState({
            success: "Successfully added university to database",
            universityFormData: {
              name: "",
              address: "",
              email: "",
              phone: ""
            }
          });
          setTimeout(() => {
            this.setState({ success: "" });
          }, 4000);
        }
      })
      .catch(err => {
        this.setState({
          errors: ["Something went wrong. Check console for more information"]
        });
        console.log(err);
      });
  };

  inputHandler = e => {
    let universityFormData = { ...this.state.universityFormData };
    universityFormData[e.target.id] = e.target.value;
    this.setState({ universityFormData });
  };

  render() {
    const { name, address, email, phone } = this.state.universityFormData;
    const { errors, success } = this.state;
    return (
      <Container>
        <Form onSubmit={this.submitHandler}>
          <Form.Group as={Row}>
            <Form.Label column lg="2">
              University name
            </Form.Label>
            <Col lg="10">
              <Form.Control
                id="name"
                name="name"
                type="text"
                onChange={this.inputHandler}
                value={name}
                placeholder="Politechnika Śląska w Gliwicach"
              />
            </Col>
          </Form.Group>
          <Form.Group as={Row}>
            <Form.Label column lg="2">
              Address
            </Form.Label>
            <Col lg="10">
              <Form.Control
                id="address"
                name="address"
                type="text"
                onChange={this.inputHandler}
                value={address}
                placeholder="Akademicka 2A, 44-100 Gliwice"
              />
            </Col>
          </Form.Group>
          <Form.Group as={Row}>
            <Form.Label column lg="2">
              Email
            </Form.Label>
            <Col lg="10">
              <Form.Control
                id="email"
                name="email"
                type="email"
                onChange={this.inputHandler}
                value={email}
                placeholder="example@mail.com"
              />
            </Col>
          </Form.Group>
          <Form.Group as={Row}>
            <Form.Label column lg="2">
              Phone
            </Form.Label>
            <Col lg="10">
              <Form.Control
                id="phone"
                name="phone"
                type="text"
                onChange={this.inputHandler}
                value={phone}
                placeholder="765-831-932"
              />
            </Col>
          </Form.Group>

          {errors.length > 0 && (
            <Alert variant="warning">
              {errors.map(err => {
                return (
                  <li
                    key={Math.random()
                      .toString(36)
                      .substr(2, 9)}
                  >
                    {err}
                  </li>
                );
              })}
            </Alert>
          )}
          {success.length > 0 && <Alert variant="success">{success}</Alert>}
          <Row>
            <Col lg={{span:4, offset:2}}>
              <Button variant="success" type="submit">
                Add University
              </Button>
            </Col>
          </Row>
        </Form>
      </Container>
    );
  }
}

export default AddUniversity;
