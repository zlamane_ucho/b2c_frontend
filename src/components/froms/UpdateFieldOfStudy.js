import React, { Component } from "react";
import { Alert, Button, Col, Row, Container } from "react-bootstrap";
import Select from "react-select";
import UpdateFieldForm from "./formComponents/UpdateFieldForm";
import _ from "lodash";

class UpdateFieldOfStudy extends Component {
  constructor(props) {
    super(props);

    this.state = {
      fieldsOption: null,
      fields: [],
      fieldsData: {},
      currentlyChangingField: {},
      loading: true,
      errors: [],
      validationErrors: [],
      success: ""
    };
  }
  componentDidMount() {
    this.setState({ loading: true });
    this.getFields();
  }

  getFields = () => {
    this.setState({ loading: true });
    fetch("/fieldofstudy/all")
      .then(res => {
        if (!res.ok) throw Error(res.statusText);
        return res.json();
      })
      .then(fields => {
        const mapedFields = fields.map(field => {
          return {
            value: field.id,
            label: field.nameOfStudyField
          };
        });
        const fieldsData = {};
        fields.forEach(field => {
          field.startDate = field.startDate.substr(0,10);
          fieldsData[field.id] = field;
        });

        if (mapedFields.length) {
          this.setState({
            fields: mapedFields,
            fieldsOption: mapedFields[0],
            fieldsData,
            loading: false,
            errors: []
          });
        } else {
          this.setState({
            fieldsOption: null,
            fields: [],
            fieldsData: {},
            loading: false,
            errors: []
          });
        }
      })
      .catch(err => {
        console.log(err);
        const errors = [...this.state.errors, "Couldn't fetched fields of study"];
        this.setState({ errors, loading: false });
      });
  };

  fieldHandler = fieldsOption => {
    const currentlyChangingField = { ...this.state.currentlyChangingField };
    const { value } = this.state.fieldsOption;
    let fieldsData = { ...this.state.fieldsData };
    if (Object.keys(currentlyChangingField).length)
      fieldsData[value] = currentlyChangingField;

    this.setState((state, props) => {
      return {
        fieldsOption,
        currentlyChangingField: {},
        fieldsData,
        validationErrors: []
      };
    });
  };

  inputHandler = e => {
    //get ID
    const currentFieldId = this.state.fieldsOption.value;
    //get field data base on ID
    let currentFieldData = {
      ...this.state.fieldsData[currentFieldId]
    };
    //save currently changing field for later equality check
    let currentlyChangingField = { ...this.state.currentlyChangingField };
    if (!Object.keys(currentlyChangingField).length)
      currentlyChangingField = { ...currentFieldData };
    //change field data
    currentFieldData[e.target.id] = e.target.value;
    //get obj with all fields
    let fieldsData = { ...this.state.fieldsData };
    //push changed field to it
    fieldsData[currentFieldId] = currentFieldData;
    //set new fields data
    this.setState(state => {
      return { ...state, fieldsData, currentlyChangingField };
    });
  };

  updateField = () => {
    const currentFieldId = this.state.fieldsOption.value;
    const dataToSend = this.state.fieldsData[currentFieldId];
    const dataEqalityCheck = this.state.currentlyChangingField;

    const objHasKey = Object.keys(dataEqalityCheck).length > 0;
    if (objHasKey === _.isEqual(dataToSend, dataEqalityCheck)) {
      this.setState(state => {
        return {
          ...state,
          validationErrors: ["You didn't change anything!"]
        };
      });
      setTimeout(() => {
        this.setState({ validationErrors: [] });
      }, 2500);
      return;
    }

    let validationErrors = this.validateFieldsForm(dataToSend);
    if (validationErrors.length) {
      this.setState({ validationErrors });
      return;
    }

    fetch("/fieldofstudy/add", {
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json"
      },
      method: "POST",
      body: JSON.stringify([dataToSend])
    })
      .then(res => {
        if (!res.ok) throw Error(res.statusText);
        return res.json();
      })
      .then(field => {
        if (field.nameOfStudyField) {
          this.setState({
            success: `Successfully updated: ${field.nameOfStudyField}`,
            currentlyChangingField: {},
            validationErrors: []
          });
        }
        this.getFields();
        setTimeout(() => {
          this.setState({ success: "" });
        }, 2500);
      })
      .catch(err => {
        console.log(err);
        this.setState({ errors: ["Something went wrong with update"] });
      });
  };

  validateFieldsForm = ({ nameOfStudyField, startDate, studentsLimit }) => {
    let errors = [];
    if (!nameOfStudyField) errors.push("Add field name");
    if (!startDate) errors.push("Add field start date");
    if (!studentsLimit) errors.push("Add field students limit");
    if (!/^[0-9]*$/.test(studentsLimit)) errors.push("Wrong students limit. Only whole positive numbers.");

    return errors;
  };

  render() {
    const {
      fields,
      loading,
      fieldsOption,
      fieldsData,
      errors,
      success,
      validationErrors
    } = this.state;

    let fieldsSelect = null;
    if (fields.length && !errors.length) {
      const { nameOfStudyField, startDate, studentsLimit } = fieldsData[
        fieldsOption.value
      ];
      fieldsSelect = (
        <Container>
          <Row className="justify-content-center">
            <Col lg="3">
              <Button onClick={this.updateField} variant="success" block>
                UPDATE FIELD
              </Button>
            </Col>
            <Col lg="6">
              <Select
                value={fieldsOption}
                onChange={this.fieldHandler}
                options={fields}
              />
            </Col>
          </Row>
          <Row className="justify-content-center">
            <Col lg="9">
              <UpdateFieldForm
                data={{ nameOfStudyField, startDate, studentsLimit }}
                inputHandler={this.inputHandler}
              />
            </Col>
          </Row>
        </Container>
      );
    } else {
      fieldsSelect = (
        <Alert variant="success">Add some fields</Alert>
      );
    }
    if (loading)
      fieldsSelect = (
        <Alert variant="success">Loading fields... Please wait.</Alert>
      );
    if (errors.length)
      fieldsSelect = (
        <Alert variant="danger">
          {errors.map(err => {
            return (
              <li
                key={Math.random()
                  .toString(36)
                  .substr(2, 9)}
              >
                {err}
              </li>
            );
          })}
        </Alert>
      );

    return (
      <Container>
        {fieldsSelect}
        <Row className="justify-content-center">
          <Col lg="9">
            {validationErrors.length > 0 && (
              <Alert variant="warning">
                {validationErrors.map(err => {
                  return (
                    <li
                      key={Math.random()
                        .toString(36)
                        .substr(2, 9)}
                    >
                      {err}
                    </li>
                  );
                })}
              </Alert>
            )}
            {success.length > 0 && <Alert variant="success">{success}</Alert>}
          </Col>
        </Row>
      </Container>
    );
  }
}

export default UpdateFieldOfStudy;
