import React, { Component } from "react";
import { Container, Row, Col, Alert, Button } from "react-bootstrap";
import _ from "lodash";
import Select from "react-select";
import { Route, Link } from "react-router-dom";
import UpdateStudentData from "./formComponents/UpdateStudentData";
import UpdateStudentFields from "./formComponents/UpdateStudentFields";

class UpdateStudent extends Component {
  constructor(props) {
    super(props);

    this.state = {
      studentsOption: null,
      students: [],
      studentsData: {},
      currentlyChangingData: {},
      loading: true,
      errors: [],
      validationErrors: [],
      success: ""
    };
  }
  componentDidMount() {
    this.getStudents();
  }

  inputHandler = e => {
    let studentsData = { ...this.state.studentsData };
    const currentStudentId = this.state.studentsOption.value;
    let studentData = { ...studentsData[currentStudentId] };

    let currentlyChangingData = { ...this.state.currentlyChangingData };
    if (!Object.keys(currentlyChangingData).length)
      currentlyChangingData = { ...studentData };

    //AGE -> ONLY WHOLE POSITIVE VALUES
    if (e.target.id === "age" && !/^[0-9]*$/.test(e.target.value)) return;

    studentsData[currentStudentId][e.target.id] = e.target.value;
    this.setState({ studentsData, currentlyChangingData });
  };

  getStudents = () => {
    this.setState({ loading: true });
    fetch("/student/all")
      .then(res => {
        if (!res.ok) throw Error(res.statusText);
        return res.json();
      })
      .then(students => {
        const mapedStudents = students.map(student => {
          return {
            value: student.id,
            label: `${student.firstName} ${student.lastName}`
          };
        });
        const studentsData = {};
        students.forEach(student => {
          studentsData[student.id] = student;
        });

        if (mapedStudents.length) {
          this.setState({
            students: mapedStudents,
            studentsOption: mapedStudents[0],
            studentsData,
            loading: false,
            errors: []
          });
        } else {
          this.setState({
            studentsOption: null,
            students: [],
            studentsData: {},
            loading: false,
            errors: []
          });
        }
      })
      .catch(err => {
        console.log(err);
        const errors = [...this.state.errors, "Couldn't fetched students"];
        this.setState({ errors, loading: false });
      });
  };

  studentHandler = studentsOption => {
    const currentlyChangingData = { ...this.state.currentlyChangingData };
    const { value } = this.state.studentsOption;
    let studentsData = { ...this.state.studentsData };
    if (Object.keys(currentlyChangingData).length)
      studentsData[value] = currentlyChangingData;
    this.setState({ studentsOption, currentlyChangingData: {}, studentsData });
  };

  submitDataHandler = e => {
    e.preventDefault();

    const currentStudentId = this.state.studentsOption.value;
    const dataToSend = this.state.studentsData[currentStudentId];
    const dataEqualityCheck = this.state.currentlyChangingData;

    const objHasKey = Object.keys(dataEqualityCheck).length > 0;
    if (objHasKey === _.isEqual(dataToSend, dataEqualityCheck)) {
      this.setState(state => {
        return {
          ...state,
          validationErrors: ["You didn't change anything!"]
        };
      });
      setTimeout(() => {
        this.setState(state => {
          return {
            ...state,
            validationErrors: []
          };
        });
      }, 2500);
      return;
    }
    const errors = this.validateStudentData(dataToSend);
    if (errors.length) {
      this.setState({ validationErrors: errors });
      return;
    }
    this.updateStudentData(dataToSend);
  };

  validateStudentData = ({ firstName, lastName, email, phone, age }) => {
    let errors = [];
    if (!firstName) errors.push("Add student's first name");
    if (!lastName) errors.push("Add student's lat name");
    if (!email) errors.push("Add student's email");
    if (!/^\w+@\w+\.\w{2,3}$/.test(email)) errors.push("Incorrect email");
    if (!age) errors.push("Add student's age");
    if (age < 18 || age > 100) errors.push("Age from 18 to 100");
    if (!/^\d{3}-?\d{3}-?\d{3}$/.test(phone))
      errors.push("Wrong phone number. Correct form: 123456789 or 123-456-789");

    return errors;
  };

  updateStudentData = dataToSend => {
    fetch("/student/update", {
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json"
      },
      method: "POST",
      body: JSON.stringify(dataToSend)
    })
      .then(res => {
        if (!res.ok) throw Error(res.statusText);

        return res.json();
      })
      .then(student => {
        this.setState({
          success: `Successfully updated ${student.firstName} ${
            student.lastName
          }`,
          currentlyChangingData: {},
          validationErrors: []
        });
        setTimeout(() => {
          this.setState({ success: "" });
        }, 2500);
        this.getStudents();
      })
      .catch(err => {
        console.log(err);
        this.setState({
          errors: ["Something went wrong. Could't update student."]
        });
      });
  };

  render() {
    const {
      students,
      studentsOption,
      loading,
      errors,
      validationErrors,
      success
    } = this.state;
    let studentData;
    if (students.length)
      studentData = {
        ...this.state.studentsData[studentsOption.value]
      };

    let studentsSelect = null;
    if (students.length) {
      studentsSelect = (
        <div>
          <Row className="justify-content-center">
            <Col lg="10">
              <Select
                value={studentsOption}
                onChange={this.studentHandler}
                options={students}
              />
            </Col>
          </Row>
          <Row className="justify-content-center">
            <Col lg="5">
              <Link to="/update/student/data">
                <Button variant="secondary" block>
                  DATA
                </Button>
              </Link>
            </Col>
            <Col lg="5">
              <Link to="/update/student/fields">
                <Button variant="secondary" block>
                  FIELDS
                </Button>
              </Link>
            </Col>
          </Row>
        </div>
      );
    } else {
      studentsSelect = <Alert variant="warning">Add some students</Alert>;
    }
    if (loading)
      studentsSelect = (
        <Alert variant="success">Loading students... Please wait.</Alert>
      );
    if (errors.length)
      studentsSelect = (
        <Alert variant="danger">
          {errors.map(err => {
            return (
              <li
                key={Math.random()
                  .toString(36)
                  .substr(2, 9)}
              >
                {err}
              </li>
            );
          })}
        </Alert>
      );
    return (
      <Container>
        {studentsSelect}
        <Row className="justify-content-center">
          <Col lg="12">
            {studentData && (
              <Route
                path="/update/student/data"
                render={props => (
                  <UpdateStudentData
                    {...props}
                    data={studentData}
                    inputHandler={this.inputHandler}
                    submitHandler={this.submitDataHandler}
                    validationErrors={validationErrors}
                    success={success}
                  />
                )}
              />
            )}
            {studentsOption && (
              <Route
                path="/update/student/fields"
                render={props => (
                  <UpdateStudentFields
                    {...props}
                    studentId={studentsOption.value}
                  />
                )}
              />
            )}
          </Col>
        </Row>
        <Row>{}</Row>
      </Container>
    );
  }
}

export default UpdateStudent;
