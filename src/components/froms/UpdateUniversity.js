import React, { Component } from "react";
import { Alert, Button, Col, Row, Container } from "react-bootstrap";
import Select from "react-select";
import UpdateUniversityForm from "./formComponents/UpdateUniversityForm";
import _ from "lodash";

class UpdateUniversity extends Component {
  constructor(props) {
    super(props);

    this.state = {
      universitiesOption: null,
      universities: [],
      universitiesData: {},
      currentlyChangingUni: {},
      loading: true,
      errors: [],
      validationErrors: [],
      success: ""
    };
  }
  componentDidMount() {
    this.setState({ loading: true });
    this.getUniversities();
  }

  getUniversities = () => {
    this.setState({ loading: true });
    fetch("/university/all")
      .then(res => {
        if (!res.ok) throw Error(res.statusText);
        return res.json();
      })
      .then(universities => {
        const mapedUniversities = universities.map(university => {
          return {
            value: university.id,
            label: university.name
          };
        });
        const universitiesData = {};
        universities.forEach(university => {
          universitiesData[university.id] = university;
        });

        if (mapedUniversities.length) {
          this.setState({
            universities: mapedUniversities,
            universitiesOption: mapedUniversities[0],
            universitiesData,
            loading: false,
            errors: []
          });
        } else {
          this.setState({
            universitiesOption: null,
            universities: [],
            universitiesData: {},
            loading: false,
            errors: []
          });
        }
      })
      .catch(err => {
        console.log(err);
        const errors = [...this.state.errors, "Couldn't fetched univeristies"];
        this.setState({ errors, loading: false });
      });
  };

  universityHandler = universitiesOption => {
    const currentlyChangingUni = { ...this.state.currentlyChangingUni };
    const { value } = this.state.universitiesOption;
    let universitiesData = { ...this.state.universitiesData };
    if (Object.keys(currentlyChangingUni).length)
      universitiesData[value] = currentlyChangingUni;

    this.setState((state, props) => {
      return {
        universitiesOption,
        currentlyChangingUni: {},
        universitiesData,
        validationErrors: []
      };
    });
  };

  inputHandler = e => {
    //get ID
    const currentUniversityId = this.state.universitiesOption.value;
    //get university data base on ID
    let currentUniversityData = {
      ...this.state.universitiesData[currentUniversityId]
    };
    //save currently changing uni for later equality check
    let currentlyChangingUni = { ...this.state.currentlyChangingUni };
    if (!Object.keys(currentlyChangingUni).length)
      currentlyChangingUni = { ...currentUniversityData };
    //change university data
    currentUniversityData[e.target.id] = e.target.value;
    //get obj with all universities
    let universitiesData = { ...this.state.universitiesData };
    //push changed university to it
    universitiesData[currentUniversityId] = currentUniversityData;
    //set new universities data
    this.setState(state => {
      return { ...state, universitiesData, currentlyChangingUni };
    });
  };

  updateUniversity = () => {
    const currentUniversityId = this.state.universitiesOption.value;
    const dataToSend = this.state.universitiesData[currentUniversityId];
    const dataEqualityCheck = this.state.currentlyChangingUni;

    const objHasKey = Object.keys(dataEqualityCheck).length > 0;
    if (objHasKey === _.isEqual(dataToSend, dataEqualityCheck)) {
      this.setState(state => {
        return {
          ...state,
          validationErrors: ["You didn't change anything!"]
        };
      });
      setTimeout(() => {
        this.setState({ validationErrors: [] });
      }, 2500);
      return;
    }

    let validationErrors = this.validateUniversityForm(dataToSend);
    if (validationErrors.length) {
      this.setState({ validationErrors });
      return;
    }

    fetch("/university/add", {
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json"
      },
      method: "POST",
      body: JSON.stringify(dataToSend)
    })
      .then(res => {
        if (!res.ok) throw Error(res.statusText);
        return res.json();
      })
      .then(university => {
        if (university.name) {
          this.setState({
            success: `Successfully updated: ${university.name}`,
            currentlyChangingUni: {},
            validationErrors: []
          });
        }
        this.getUniversities();
        setTimeout(() => {
          this.setState({ success: "" });
        }, 2500);
      })
      .catch(err => {
        console.log(err);
        this.setState({ errors: ["Something went wrong with update"] });
      });
  };

  validateUniversityForm = ({ name, address, email, phone }) => {
    let errors = [];
    if (!name) errors.push("Add university name");
    if (!address) errors.push("Add university adress");
    if (!email) errors.push("Add university email");
    if (!/^\w+@\w+\.\w{2,3}$/.test(email)) errors.push("Incorrect email");
    if (!/^\d{3}-?\d{3}-?\d{3}$/.test(phone))
      errors.push("Wrong phone number. Correct form: 123456789 or 123-456-789");

    return errors;
  };

  render() {
    const {
      universities,
      loading,
      universitiesOption,
      universitiesData,
      errors,
      success,
      validationErrors
    } = this.state;

    let universitiesSelect = null;
    if (universities.length && !errors.length) {
      const { name, address, email, phone } = universitiesData[
        universitiesOption.value
      ];
      universitiesSelect = (
        <Container>
          <Row className="justify-content-center">
            <Col lg="3">
              <Button onClick={this.updateUniversity} variant="success" block>
                UPDATE UNIVERSITY
              </Button>
            </Col>
            <Col lg="6">
              <Select
                value={universitiesOption}
                onChange={this.universityHandler}
                options={universities}
              />
            </Col>
          </Row>
          <Row className="justify-content-center">
            <Col lg="9">
              <UpdateUniversityForm
                data={{ name, address, email, phone }}
                inputHandler={this.inputHandler}
              />
            </Col>
          </Row>
        </Container>
      );
    } else {
      universitiesSelect = (
        <Alert variant="success">Add some universities</Alert>
      );
    }
    if (loading)
      universitiesSelect = (
        <Alert variant="success">Loading universities... Please wait.</Alert>
      );
    if (errors.length)
      universitiesSelect = (
        <Alert variant="danger">
          {errors.map(err => {
            return (
              <li
                key={Math.random()
                  .toString(36)
                  .substr(2, 9)}
              >
                {err}
              </li>
            );
          })}
        </Alert>
      );

    return (
      <Container>
        {universitiesSelect}
        <Row className="justify-content-center">
          <Col lg="9">
            {validationErrors.length > 0 && (
              <Alert variant="warning">
                {validationErrors.map(err => {
                  return (
                    <li
                      key={Math.random()
                        .toString(36)
                        .substr(2, 9)}
                    >
                      {err}
                    </li>
                  );
                })}
              </Alert>
            )}
            {success.length > 0 && <Alert variant="success">{success}</Alert>}
          </Col>
        </Row>
      </Container>
    );
  }
}

export default UpdateUniversity;
