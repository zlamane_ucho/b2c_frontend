import React from "react";
import { Button, ListGroup } from "react-bootstrap";

const FieldOfStudyBar = props => {
  return (
    <ListGroup.Item>
      {props.name} ({props.studentsLimit}) / start date: {props.startDate}
      <Button
        variant="danger"
        size="sm"
        value={props.id}
        onClick={props.delete}
        className="btn-hor-mar"
      >
        Delete
      </Button>
    </ListGroup.Item>
  );
};

export default FieldOfStudyBar;
