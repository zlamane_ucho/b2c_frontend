import React from "react";
import { Form, Col, Row } from "react-bootstrap";

const UpdateFieldForm = props => {
  const { nameOfStudyField, startDate, studentsLimit } = props.data;
  const { inputHandler } = props;
  return (
    <Form>
      <Form.Group as={Row}>
        <Form.Label column lg="4">
          Name of field
        </Form.Label>
        <Col lg="8">
          <Form.Control
            id="nameOfStudyField"
            name="nameOfStudyField"
            type="text"
            value={nameOfStudyField}
            onChange={inputHandler}
            placeholder="Computer Programming"
          />
        </Col>
      </Form.Group>
      <Form.Group as={Row}>
        <Form.Label column lg="2">
          Start date
        </Form.Label>
        <Col lg="4">
          <Form.Control
            id="startDate"
            name="startDate"
            type="date"
            value={startDate}
            onChange={inputHandler}
            placeholder="01-10-2019"
          />
        </Col>
        <Form.Label column lg="3">
          Students' limit
        </Form.Label>
        <Col lg="3">
          <Form.Control
            id="studentsLimit"
            name="studentsLimit"
            type="text"
            value={studentsLimit}
            onChange={inputHandler}
            placeholder="30"
          />
        </Col>
      </Form.Group>
    </Form>
  );
};

export default UpdateFieldForm;
