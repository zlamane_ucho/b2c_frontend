import React, { Component } from "react";
import { Form, Button, Row, Col, Alert } from "react-bootstrap";

class UpdateStudentData extends Component {
  render() {
    const {
      firstName,
      lastName,
      email,
      phone,
      age,
      gender,
      studentStatus
    } = this.props.data;
    const {
      inputHandler,
      submitHandler,
      validationErrors,
      success
    } = this.props;
    return (
      <Form onSubmit={submitHandler}>
        <Form.Group as={Row}>
          <Form.Label column lg="2">
            First name
          </Form.Label>
          <Col lg="4">
            <Form.Control
              id="firstName"
              name="firstName"
              type="text"
              onChange={inputHandler}
              value={firstName}
              placeholder="First name"
            />
          </Col>
          <Form.Label column lg="2">
            Last name
          </Form.Label>
          <Col lg="4">
            <Form.Control
              id="lastName"
              name="lastName"
              type="text"
              onChange={inputHandler}
              value={lastName}
              placeholder="Last name"
            />
          </Col>
        </Form.Group>
        <Form.Group as={Row}>
          <Form.Label column lg="2">
            Email
          </Form.Label>
          <Col lg="4">
            <Form.Control
              id="email"
              name="email"
              type="email"
              onChange={inputHandler}
              value={email}
              placeholder="example@mail.com"
            />
          </Col>
          <Form.Label column lg="2">
            Phone number
          </Form.Label>
          <Col lg="4">
            <Form.Control
              id="phone"
              name="phone"
              type="text"
              onChange={inputHandler}
              value={phone}
              placeholder="765-831-932"
            />
          </Col>
        </Form.Group>
        <Form.Group as={Row}>
          <Form.Label column lg="2">
            Age
          </Form.Label>
          <Col lg="4">
            <Form.Control
              id="age"
              name="age"
              type="text"
              onChange={inputHandler}
              value={age}
              placeholder="Age"
            />
          </Col>
          <Form.Label column lg="2">
            Gender
          </Form.Label>
          <Col lg="4">
            <Form.Control
              id="gender"
              name="gender"
              as="select"
              onChange={inputHandler}
              value={gender}
            >
              <option value="MALE">Male</option>
              <option value="FEMALE">Female</option>
            </Form.Control>
          </Col>
        </Form.Group>
        <Form.Group as={Row}>
          <Form.Label column lg="2">
            Status
          </Form.Label>
          <Col lg="4">
            <Form.Control
              id="studentStatus"
              name="studentStatus"
              as="select"
              onChange={inputHandler}
              value={studentStatus}
            >
              <option value="ACTIVE">Active</option>
              <option value="INACTIVE">Inactive</option>
              <option value="SUSPENDED">Suspended</option>
              <option value="DEANS_LEAVE">Dean's Leave</option>
            </Form.Control>
          </Col>
          <Col lg={{ span: 4, offset: 2 }}>
            <Button type="submit" variant="success">
              UPDATE DATA
            </Button>
          </Col>
        </Form.Group>

        <Row className="justify-content-center">
          <Col lg="12">
            {validationErrors.length > 0 && (
              <Alert variant="warning">
                {validationErrors.map(err => {
                  return (
                    <li
                      key={Math.random()
                        .toString(36)
                        .substr(2, 9)}
                    >
                      {err}
                    </li>
                  );
                })}
              </Alert>
            )}
            {success.length > 0 && <Alert variant="success">{success}</Alert>}
          </Col>
        </Row>
      </Form>
    );
  }
}

export default UpdateStudentData;
