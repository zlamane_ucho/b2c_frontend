import React, { Component } from "react";
import {
  InputGroup,
  FormControl,
  Button,
  Form,
  Alert,
  Container,
  Col,
  Row
} from "react-bootstrap";
import _ from "lodash";

class UpdateStudentFields extends Component {
  constructor(props) {
    super(props);

    this.state = {
      possibleFields: [],
      studentFields: {},
      choosedFields: [],
      grades: {},
      errors: [],
      success: ""
    };
  }
  componentDidMount() {
    const { studentId } = this.props;
    this.getPossibleFields(studentId);
  }
  componentWillReceiveProps(nextProps) {
    if (nextProps.studentId !== this.props.studentId)
      this.getPossibleFields(nextProps.studentId);
  }

  getPossibleFields = studentId => {
    return fetch(`/fieldofstudy/possiblefields/${studentId}`)
      .then(res => {
        if (!res.ok) throw Error(res.statusText);
        return res.json();
      })
      .then(fields => {
        this.setState({ possibleFields: fields });
        this.getStudentFields(studentId);
      })
      .catch(err => console.log(err));
  };
  getStudentFields = studentId => {
    fetch(`/studentsGrades/studentfields/${studentId}`)
      .then(res => {
        if (!res.ok) throw Error(res.statusText);
        return res.json();
      })
      .then(fields => {
        let studentFields = {};
        let grades = {};
        fields.forEach(field => {
          studentFields[field.fieldId] = field;
          grades[field.fieldId] = field.grade;
        });
        const choosedFields = fields.map(field => {
          return field.fieldId;
        });

        this.setState({ studentFields, grades, choosedFields });
      })
      .catch(err => console.log(err));
  };

  fieldsHandler = e => {
    let choosedFields = [...this.state.choosedFields];
    if (e.target.checked) {
      choosedFields.push(e.target.value);
    } else {
      choosedFields.splice(choosedFields.indexOf(e.target.value), 1);
    }
    this.setState({ choosedFields });
  };

  gradesHandler = e => {
    const value = e.target.value;
    //TODO Grade error
    //Grade range 2.0 - 5.0 changing by 0.5
    if (!/^(2|3|4|5)\.?(0|5)?$/.test(value) && !/$^/.test(value)) return;
    if (Number.parseFloat(value, 10) > 5.0 || value.length > 3) return;
    let grades = { ...this.state.grades };
    grades[e.target.id] = value;
    this.setState({ grades });
  };

  prepareInputGroup = () => {
    const { possibleFields, choosedFields, grades } = this.state;
    return possibleFields.map(field => {
      return (
        <Col lg="6">
          <InputGroup key={field.id} className="mb-3">
            <InputGroup.Prepend>
              <InputGroup.Checkbox
                onChange={this.fieldsHandler}
                value={field.id}
                checked={choosedFields.includes(field.id)}
              />
              <InputGroup.Text>{field.nameOfStudyField}</InputGroup.Text>
            </InputGroup.Prepend>
            <FormControl
              onChange={this.gradesHandler}
              id={field.id}
              value={grades.hasOwnProperty(field.id) ? grades[field.id] : ""}
              placeholder="Grade e.g. 4.5"
            />
          </InputGroup>
        </Col>
      );
    });
  };

  submitHandler = e => {
    e.preventDefault();
    if (!this.checkIfSomethingIsChanged()) {
      this.setState({ errors: ["You didn't change anything!"] });
      return;
    }
    let noGrade = false;
    const { studentId } = this.props;
    const { grades, choosedFields } = this.state;
    const studentGrades = choosedFields.map(field => {
      if (!grades[field]) noGrade = true;
      return {
        fieldId: field,
        grade: grades[field],
        studentId
      };
    });
    if (noGrade) {
      this.setState({ errors: ["Add grade to every checked field!"] });
      return;
    } else {
      this.setState({ errors: [] });
    }

    this.updateStudentFields(studentGrades, studentId);
  };

  checkIfSomethingIsChanged = () => {
    const { studentFields, choosedFields, grades } = this.state;
    const studentChecked = Object.keys(studentFields);

    if (_.isEqual(studentChecked, choosedFields)) {
      let changed = false;
      choosedFields.forEach(field => {
        if (grades[field] !== studentFields[field].grade) changed = true;
      });
      return changed;
    }
    return true;
  };

  updateStudentFields = (studentGrades, studentId) => {
    fetch(`/studentsGrades/delete/${studentId}`, {
      method: "DELETE"
    })
      .then(res => {
        if (!res.ok) throw Error(res.statusText);

        return res.json();
      })
      .then(data => {
        if (data.deleted) {
          fetch("/studentsGrades/add", {
            headers: {
              Accept: "application/json",
              "Content-Type": "application/json"
            },
            method: "POST",
            body: JSON.stringify(studentGrades)
          })
            .then(res => {
              if (!res.ok) throw Error(res.statusText);
              return res.json();
            })
            .then(studentGrades => {
              const studentFields = {};
              const grades = {};
              studentGrades.forEach(grade => {
                studentFields[grade.fieldId] = grade;
                grades[grade.fieldId] = grade.grade;
              });
              const choosedFields = studentGrades.map(field => {
                return field.fieldId;
              });

              if (studentGrades) {
                this.setState({
                  success: "Successfully updated student",
                  studentFields,
                  grades,
                  choosedFields
                });
                setTimeout(() => {
                  this.setState({ success: "" });
                }, 2500);

                let avg = 0;
                studentGrades.forEach(grade => {
                  avg += grade.grade;
                });
                if (avg)
                  avg = Math.round((avg * 100) / studentGrades.length) / 100;
                fetch(`/student/newAvgGrade/${studentId}/${avg}`).then(res => {
                  if (!res.ok) throw Error(res.statusText);
                  return res.json();
                });
              }
            });
        }
      })
      .catch(err => console.log(err));
  };

  render() {
    const { possibleFields, errors, success } = this.state;

    return (
      <Container>
          {possibleFields.length ? (
            <Form as={Row} className="justify-content-center" onSubmit={this.submitHandler}>
              {this.prepareInputGroup()}
              <Button type="submit" variant="success">
                UPDATE STUDENT FIELDS
              </Button>
            </Form>
          ) : null}
          {errors.length > 0 && (
            <Alert variant="warning">
              {errors.map(err => {
                return (
                  <li
                    key={Math.random()
                      .toString(36)
                      .substr(2, 9)}
                  >
                    {err}
                  </li>
                );
              })}
            </Alert>
          )}
          {success ? <Alert variant="success">{success}</Alert> : null}
        
      </Container>
    );
  }
}

export default UpdateStudentFields;
