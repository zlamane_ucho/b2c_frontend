import React from "react";
import { Form, Col, Row } from "react-bootstrap";

const UpdateUniversityForm = props => {
  const { name, address, email, phone } = props.data;
  const { inputHandler } = props;
  return (
    <Form>
      <Form.Group as={Row}>
        <Form.Label column lg="4">
          University name
        </Form.Label>
        <Col lg="8">
          <Form.Control
            id="name"
            name="name"
            type="text"
            onChange={inputHandler}
            value={name}
            placeholder="Politechnika Śląska w Gliwicach"
          />
        </Col>
      </Form.Group>
      <Form.Group as={Row}>
        <Form.Label column lg="4">
          Address
        </Form.Label>
        <Col lg="8">
          <Form.Control
            id="address"
            name="address"
            type="text"
            onChange={inputHandler}
            value={address}
            placeholder="Akademicka 2A, 44-100 Gliwice"
          />
        </Col>
      </Form.Group>
      <Form.Group as={Row}>
        <Form.Label column lg="4">
          Email
        </Form.Label>
        <Col lg="8">
          <Form.Control
            id="email"
            name="email"
            type="email"
            onChange={inputHandler}
            value={email}
            placeholder="example@mail.com"
          />
        </Col>
      </Form.Group>
      <Form.Group as={Row}>
        <Form.Label column lg="4">
          Phone number
        </Form.Label>
        <Col lg="8">
          <Form.Control
            id="phone"
            name="phone"
            type="text"
            onChange={inputHandler}
            value={phone}
            placeholder="765-831-934"
          />
        </Col>
      </Form.Group>
    </Form>
  );
};

export default UpdateUniversityForm;
