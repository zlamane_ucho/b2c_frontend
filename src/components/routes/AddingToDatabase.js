import React, { Component } from "react";
import { Route, Link } from "react-router-dom";
import { Container, Row, Col, Button, Alert } from "react-bootstrap";

import AddUniversity from "../froms/AddUniversity";
import AddFieldsOfStudy from "../froms/AddFieldsOfStudy";
import AddStudent from "../froms/AddStudent";

class AddingToDatabase extends Component {
  componentDidMount() {
    this.props.resetColors();
  }
  render() {
    const { colors, onClick } = this.props;
    return (
      <Container>
        <Row>
          <Col lg="12">
            <Row className="justify-content-center">
              <Col lg="12">
                <Alert variant="dark">
                  <h1>Add new rows to databes</h1>
                </Alert>
              </Col>
            </Row>
          </Col>
        </Row>
        <Row>
          <Col lg="2" md="4" sm="12">
            <Link to="/add/university">
              <Button
                variant={colors[0]}
                value={0}
                onClick={onClick}
                block={true}
              >
                University
              </Button>
            </Link>
            <Link to="/add/fields">
              <Button
                variant={colors[1]}
                value={1}
                onClick={onClick}
                block={true}
              >
                Fields Of Study
              </Button>
            </Link>
            <Link to="/add/student">
              <Button
                variant={colors[2]}
                value={2}
                onClick={onClick}
                block={true}
              >
                Student
              </Button>
            </Link>
          </Col>
          <Col lg="10" md="8" sm="12">
            <Route path="/add/university" component={AddUniversity} />
            <Route path="/add/fields" component={AddFieldsOfStudy} />
            <Route path="/add/student" component={AddStudent} />
          </Col>
        </Row>
      </Container>
    );
  }
}

export default AddingToDatabase;
