import React, { Component } from "react";
import { Container, Row, Col, Alert, Jumbotron } from "react-bootstrap";
import UniversitiesDashboard from "../dashboard/UniversitiesDashboard";

class Dashboard extends Component {
  render() {
    return (
      <Container>
        <Row>
          <Col lg="6">
            <UniversitiesDashboard />
          </Col>
          <Col lg="6">
            <Jumbotron>
              <h2>Work interrupted</h2>
              <p>The work ended due to lack of time</p>
            </Jumbotron>
          </Col>
        </Row>
      </Container>
    );
  }
}

export default Dashboard;
