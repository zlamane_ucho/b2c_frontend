import React from "react";
import Select from "react-select";
import { Alert, Container, Row, Col, Button } from "react-bootstrap";

class DeletingFromDataBase extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      universitiesOption: null,
      studentsOption: null,
      fieldsOption: null,
      universities: [],
      fields: [],
      students: [],
      errors: {
        universities: false,
        students: false,
        fields: false
      },
      loading: {
        universities: true,
        students: true,
        fields: true
      }
    };
  }
  componentDidMount() {
    let loading;
    this.setState({
      loading: {
        universities: true,
        students: true,
        fields: true
      }
    });

    fetch("/university/all")
      .then(res => {
        if (!res.ok) throw Error(res.statusText);
        return res.json();
      })
      .then(universities => {
        const mapedUniversities = universities.map(uni => {
          return {
            value: uni.id,
            label: uni.name
          };
        });
        loading = { ...this.state.loading };
        loading.universities = false;

        if (mapedUniversities.length) {
          this.setState({
            universities: mapedUniversities,
            universitiesOption: mapedUniversities[0],
            loading
          });

          fetch(`/fieldofstudy/universityId/${mapedUniversities[0].value}`)
            .then(res => {
              if (!res.ok) throw Error(res.statusText);
              return res.json();
            })
            .then(fields => {
              const mapedFields = fields.map(field => {
                return {
                  value: field.id,
                  label: field.nameOfStudyField
                };
              });
              loading = { ...this.state.loading };
              loading.fields = false;

              if (mapedFields.length) {
                this.setState({
                  fields: mapedFields,
                  fieldsOption: mapedFields[0],
                  loading
                });
              } else {
                this.setState({
                  fields: [],
                  fieldsOption: null,
                  loading
                });
              }

              fetch(`/student/all`)
                .then(res => {
                  if (!res.ok) throw Error(res.statusText);
                  return res.json();
                })
                .then(students => {
                  const mapedStudents = students.map(student => {
                    return {
                      value: student.id,
                      label: `${student.firstName} ${student.lastName}`
                    };
                  });
                  loading = { ...this.state.loading };
                  loading.students = false;

                  if (mapedStudents.length) {
                    this.setState({
                      students: mapedStudents,
                      studentsOption: mapedStudents[0],
                      loading
                    });
                  } else {
                    this.setState({
                      students: [],
                      studentsOption: null,
                      loading
                    });
                  }
                });
            });
        } else {
          this.setState({
            universities: [],
            universitiesOption: null,
            loading: {
              universities: false,
              students: false,
              fields: false
            }
          });
        }
      })
      .catch(err => console.log(err));
  }
  universityHandler = universitiesOption => {
    this.setState((state, props) => {
      return { universitiesOption };
    });
    this.getFields(universitiesOption);
  };
  fieldHandler = fieldsOption => {
    this.setState({ fieldsOption });
  };
  studentHandler = studentsOption => {
    this.setState({ studentsOption });
  };

  deleteStudent = e => {
    const { studentsOption } = this.state;
    fetch(`/student/delete/${studentsOption.value}`, {
      method: "DELETE"
    })
      .then(res => {
        if (!res.ok) throw Error(res.statusText);

        return res.json();
      })
      .then(data => {
        if (data.deleted) this.getStudents();
      })
      .catch(err => console.log(err));
  };
  deleteField = e => {
    const { fieldsOption, universitiesOption } = this.state;
    fetch(`/fieldofstudy/delete/${fieldsOption.value}`, {
      method: "DELETE"
    })
      .then(res => {
        if (!res.ok) throw Error(res.statusText);

        return res.json();
      })
      .then(data => {
        if (data.deleted) this.getFields(universitiesOption);
      })
      .catch(err => console.log(err));
  };
  deleteUniversity = e => {
    const { universitiesOption } = this.state;
    fetch(`/university/delete/${universitiesOption.value}`, {
      method: "DELETE"
    })
      .then(res => {
        if (!res.ok) throw Error(res.statusText);

        return res.json();
      })
      .then(data => {
        if (data.deleted) this.getUniversities();
      })
      .catch(err => console.log(err));
  };
  getStudents = () => {
    let loading;
    loading = { ...this.state.loading, students: true };
    this.setState({ loading });

    fetch("/student/all")
      .then(res => {
        if (!res.ok) throw Error(res.statusText);
        return res.json();
      })
      .then(students => {
        const mapedStudents = students.map(student => {
          return {
            value: student.id,
            label: `${student.firstName} ${student.lastName}`
          };
        });
        loading = { ...this.state.loading, students: false };

        if (mapedStudents.length) {
          this.setState({
            students: mapedStudents,
            studentsOption: mapedStudents[0],
            loading
          });
        } else {
          this.setState({
            students: [],
            studentsOption: null,
            loading
          });
        }
      })
      .catch(err => console.log(err));
  };
  getFields = ({ value }) => {
    let loading;
    loading = { ...this.state.loading, fields: true };
    this.setState({ loading });

    fetch(`/fieldofstudy/universityId/${value}`)
      .then(res => {
        if (!res.ok) throw Error(res.statusText);
        return res.json();
      })
      .then(fields => {
        const mapedFields = fields.map(field => {
          return {
            value: field.id,
            label: field.nameOfStudyField
          };
        });

        loading = { ...this.state.loading, fields: false };
        if (mapedFields.length) {
          this.setState({
            fields: mapedFields,
            fieldsOption: mapedFields[0],
            loading
          });
        } else {
          this.setState({
            fields: [],
            fieldsOption: null,
            loading
          });
        }
      })
      .catch(err => console.log(err));
  };
  getUniversities = () => {
    let loading;
    loading = { ...this.state.loading, universities: true, fields: true };
    this.setState({ loading });

    fetch(`/university/all`)
      .then(res => {
        if (!res.ok) throw Error(res.statusText);
        return res.json();
      })
      .then(universities => {
        const mapedUniversities = universities.map(university => {
          return {
            value: university.id,
            label: university.name
          };
        });

        loading = { ...this.state.loading, universities: false };
        if (mapedUniversities.length) {
          this.setState({
            universities: mapedUniversities,
            universitiesOption: mapedUniversities[0],
            loading
          });
          this.getFields(mapedUniversities[0]);
          this.getStudents();
        } else {
          loading = {};
          this.setState({
            universitiesOption: null,
            studentsOption: null,
            fieldsOption: null,
            universities: [],
            fields: [],
            students: [],
            loading: {
              universities: false,
              students: false,
              fields: false
            }
          });
        }
      })
      .catch(err => {
        console.log(err);

        this.setState({
          loading: {
            univerities: false,
            students: false,
            fields: false
          }
        });
      });
  };
  render() {
    const {
      universitiesOption,
      fieldsOption,
      studentsOption,
      universities,
      fields,
      students,
      loading
    } = this.state;

    let universitiesSelect = null;
    if (universities.length) {
      universitiesSelect = (
        <Row>
          <Col lg="4">
            <Button onClick={this.deleteUniversity} variant="danger" block>
              DELETE UNIVERSITY
            </Button>
          </Col>
          <Col lg="8">
            <Select
              value={universitiesOption}
              onChange={this.universityHandler}
              options={universities}
            />
          </Col>
        </Row>
      );
    } else {
      universitiesSelect = (
        <Alert variant="success">No more universities to delete.</Alert>
      );
    }
    if (loading.universities)
      universitiesSelect = (
        <Alert variant="success">Loading universities... Please wait.</Alert>
      );

    let fieldsSelect = null;
    if (fields.length) {
      fieldsSelect = (
        <Row>
          <Col lg="4">
            <Button onClick={this.deleteField} variant="danger" block>
              DELETE FIELD
            </Button>
          </Col>
          <Col lg="8">
            <Select
              value={fieldsOption}
              onChange={this.fieldHandler}
              options={fields}
            />
          </Col>
        </Row>
      );
    } else {
      fieldsSelect = <Alert variant="success">No more fields to delete.</Alert>;
    }
    if (loading.fields)
      fieldsSelect = (
        <Alert variant="success">Loading fields... Please wait.</Alert>
      );

    let studentsSelect = null;
    if (students.length) {
      studentsSelect = (
        <Row>
          <Col lg="4">
            <Button onClick={this.deleteStudent} variant="danger" block>
              DELETE STUDENT
            </Button>
          </Col>
          <Col lg="8">
            <Select
              value={studentsOption}
              onChange={this.studentHandler}
              options={students}
            />
          </Col>
        </Row>
      );
    } else {
      studentsSelect = (
        <Alert variant="success">No more students to delete.</Alert>
      );
    }
    if (loading.students)
      studentsSelect = (
        <Alert variant="success">Loading students... Please wait.</Alert>
      );

    return (
      <Container>
        <Row>
          <Col lg="12">
            <Row className="justify-content-center">
              <Col lg="12">
                <Alert variant="dark">
                  <h1>Delete rows from databes</h1>
                </Alert>
              </Col>
            </Row>
          </Col>
        </Row>
        {universitiesSelect}
        {fieldsSelect}
        {studentsSelect}
      </Container>
    );
  }
}

export default DeletingFromDataBase;
