import React, { Component } from "react";
import { Route, Link } from "react-router-dom";
import { Alert, Container, Row, Col, Button } from "react-bootstrap";

import UpdateUniversity from "../froms/UpdateUniversity";
import UpdateFieldOfStudy from "../froms/UpdateFieldOfStudy";
import UpdateStudent from "../froms/UpdateStudent";

class UpdatingDatabase extends Component {
  componentDidMount() {
    this.props.resetColors();
  }

  render() {
    const { colors, onClick } = this.props;
    return (
      <Container>
        <Row>
          <Col lg="12">
            <Row className="justify-content-center">
              <Col lg="12">
                <Alert variant="dark">
                  <h1>Update existing rows in databes</h1>
                </Alert>
              </Col>
            </Row>
          </Col>
        </Row>
        <Row>
          <Col lg="2" md="4" sm="12">
            <Link to="/update/university">
              <Button
                variant={colors[0]}
                value={0}
                onClick={onClick}
                block={true}
              >
                University
              </Button>
            </Link>
            <Link to="/update/fields">
              <Button
                variant={colors[1]}
                value={1}
                onClick={onClick}
                block={true}
              >
                Fields Of Study
              </Button>
            </Link>
            <Link to="/update/student">
              <Button
                variant={colors[2]}
                value={2}
                onClick={onClick}
                block={true}
              >
                Student
              </Button>
            </Link>
          </Col>
          <Col lg="10" md="8" sm="12">
            <Route path="/update/university" component={UpdateUniversity} />
            <Route path="/update/fields" component={UpdateFieldOfStudy} />
            <Route path="/update/student" component={UpdateStudent} />
          </Col>
        </Row>
      </Container>
    );
  }
}

export default UpdatingDatabase;
